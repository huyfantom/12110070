﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Nội dung TAG không được bỏ trống")]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "Nội dung TAG phải có từ 10 đến 100 kí tự")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}
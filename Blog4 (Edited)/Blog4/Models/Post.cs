﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Tiêu đề không được bỏ trống")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Tiêu đề phải có từ 20 đến 500 kí tự")]
        public string Title { set; get; }
        [Required(ErrorMessage = "Nội dung bài viết không được bỏ trống")]
        [StringLength(50, ErrorMessage = "Nội dung bài viết phải tối thiểu 50 kí tự")]
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
    }
}
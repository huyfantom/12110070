﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Post
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
    }
}
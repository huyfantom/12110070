namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class web2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false, maxLength: 50));
        }
    }
}

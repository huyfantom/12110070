﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Display(Name = "Tiêu đề bài viết")]
        [Required(ErrorMessage = "Tiêu đề không được bỏ trống")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Tiêu đề phải có từ 10 đến 500 kí tự")]
        public string Title { set; get; }
        [Display(Name = "Nội dung bài viết")]
        [Required(ErrorMessage = "Nội dung bài viết không được bỏ trống")]
        [StringLength(5000, MinimumLength=50, ErrorMessage = "Nội dung bài viết phải tối thiểu 50 kí tự")]
        public string Body { set; get; }
        [Display(Name = "Ngày đăng")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
    }
}
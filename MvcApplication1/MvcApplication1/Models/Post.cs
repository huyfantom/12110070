﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
namespace MvcApplication1.Models
{
    public class Post
    {
        [Key]
        public int id { set; get; }
        public string title { set; get; }
        public string body { set; get; }
    }
    public class BlogDBContext : DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Comment
    {
        [Key]
        public int Id { set; get; }
        public int postID { set; get; }
        public string body { set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Nội dung TAG không được bỏ trống")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "Nội dung TAG phải có từ 5 đến 100 kí tự")]
        [Display(Name = "Nội dung Tag")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}
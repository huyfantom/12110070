﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_BT2.Models
{
    public class Comment
    {
        [Key]
        public int ID { set; get; }
        [Required(ErrorMessage = "Phai nhap vai ki tu")]
        [StringLength(50, ErrorMessage = "Toi thieu co 50 ki tu")]
        public string Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateCreate { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateUpdate { set; get; }
        [Required(ErrorMessage = "Phai nhap vao ki tu")]
        public int PostID { set; get; }
        [Required(ErrorMessage = "Phai nhap vao ki tu")]
        public string Author { set; get; }
        public virtual Post Posts { set; get; }
    }
}
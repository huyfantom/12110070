﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_BT2.Models
{
    public class DefaultConnection:DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Account> Accounts { set; get; }
        public DbSet<Tag> Tags { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(d => d.Tags).WithMany(t => t.Posts).Map(m => m.MapLeftKey("PostID").MapRightKey("TagID").ToTable("TagPost"));

        }
    }
}
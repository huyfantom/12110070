﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_BT2.Models
{
    public class Tag
    {
        [Key]
        public int ID { set; get; }
        [StringLength(100, MinimumLength = 10, ErrorMessage = "Nhap 10--100 ki tu")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}
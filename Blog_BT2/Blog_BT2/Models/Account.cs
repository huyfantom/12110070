﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_BT2.Models
{
    public class Account
    {
        [Key]
        [Required(ErrorMessage = "Nhap dung Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = " Email khong dung")]
        public string Email { set; get; }
        [Required(ErrorMessage = " Pass khong dung")]
        [DataType(DataType.Password)]
        public string PassWord { set; get; }
        [StringLength(100, ErrorMessage = "Nhap toi da 10 ki tu")]
        public string FirstName { set; get; }
        [StringLength(100, ErrorMessage = "Nhap toi da 10 ki tu")]
        public string LastName { set; get; }
        public int AccountID { set; get; }
        public virtual ICollection<Post> Post { set; get; }
    }
}
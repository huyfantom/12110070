﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_BT2.Models
{
    public class Post
    {
        [Key]
        public int ID { set; get; }
        [Required(ErrorMessage = "Nhap ki tu vao ")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Title co so ki tu tu 20--500")]
        public string Title { set; get; }
        [Required(ErrorMessage = "Phai nhap ki tu")]
        [StringLength(50, ErrorMessage = "Phai nhap toi thieu 50 ki tu")]
        public string Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateUpdate { set; get; }
        [DataType(DataType.Date)]
        
        public DateTime DateCreate { set; get; }
        [Required(ErrorMessage = "Nhap Loi")]
        public int AccountID { set; get; }
        public virtual Account Accounts { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
    }
}
namespace Blog_BT2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false, maxLength: 50),
                        DateUpdate = c.DateTime(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        AccountID = c.Int(nullable: false),
                        Accounts_Email = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Accounts", t => t.Accounts_Email)
                .Index(t => t.Accounts_Email);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128),
                        PassWord = c.String(nullable: false),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        AccountID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Email);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false, maxLength: 50),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                        Author = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.TagPost",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagPost", new[] { "TagID" });
            DropIndex("dbo.TagPost", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "Accounts_Email" });
            DropForeignKey("dbo.TagPost", "TagID", "dbo.Tags");
            DropForeignKey("dbo.TagPost", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "Accounts_Email", "dbo.Accounts");
            DropTable("dbo.TagPost");
            DropTable("dbo.Comments");
            DropTable("dbo.Tags");
            DropTable("dbo.Accounts");
            DropTable("dbo.Posts");
        }
    }
}
